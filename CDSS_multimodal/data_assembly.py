#!/usr/bin/python
# -*- coding: UTF-8 -*-
import keras
import json
from tensorflow.keras import preprocessing

def pre_interview_param_get():
    print("pre_interview_param_get")
    pre_interview_vocab_file = "data/pre_interview_vocab.txt"
    word_dict = json.load(open(pre_interview_vocab_file, 'r', encoding="utf8"))
    # vocab_size = len(word_dict)
    vocabulary = word_dict.keys()
    padding_size = 130
    return word_dict,vocabulary,padding_size


def inception_param_get():
    print("inception_param_get")
    pre_interview_vocab_file = "data/inception_vocab.txt"
    word_dict = json.load(open(pre_interview_vocab_file, 'r', encoding="utf8"))
    # vocab_size = len(word_dict)
    vocabulary = word_dict.keys()
    padding_size = 250
    return word_dict,vocabulary,padding_size



def assembly(s,word_dict,vocabulary,padding_size):
    if len(s)>padding_size:
        s = s[:padding_size]

    x = [word_dict[each_word] if each_word in vocabulary else 1 for each_word in s]
    padding_length = padding_size - len(x)
    x.extend(padding_length*[0])
    # x = preprocessing.sequence.pad_sequences(x, maxlen=padding_size,padding='post', truncating='post')
    return [x]
