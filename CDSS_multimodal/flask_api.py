#!/usr/bin/python
# -*- coding: UTF-8 -*-
from data_assembly import pre_interview_param_get,inception_param_get,assembly
import requests
import json
import numpy as np
from flask import Flask, request, jsonify,Response
app = Flask(__name__)

inception_label_list = np.array(['血常规', '异淋', '超敏CRP测定', '过敏源测定', '免疫球蛋白', '补体', '胸部正位', '门诊四联呼吸道病毒快速检测', '支气管舒张试验', '流速容量曲线'])
pre_interview_label_list = np.array(['J20', 'J06', 'J18', 'J45', 'J11', 'J40', 'J00', 'R50', 'R05', 'J02', 'J21', 'J30'])

# s = "年龄8岁性别女主诉:复诊发热咳嗽（4天余）病症:患儿仍有发热，最高体温39℃，有咳嗽，阵发性连声，有喉头痰鸣，无呕吐腹泻，胃纳一般。给予奥司他韦口服4天。否认结核接触史及异物呛咳史病史:无过敏史，出生抢救史，重大疾病史，患病名称:流行性感冒体格检查:神清，精神可，咽部充血，双肺未闻及明显干湿罗音，心音中，律齐，腹部平软"
inception_param  = inception_param_get()
pre_interview_param  = pre_interview_param_get()

@app.route('/pre_interview/', methods=['POST'])
def pre_interview_predict():
    s = request.get_json().get("s")
    if s:
        s_ids = assembly(s,pre_interview_param[0],pre_interview_param[1],pre_interview_param[2])
        data = json.dumps({"inputs": {"input_data":s_ids}})
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://47.101.198.46:8501/v1/models/pre_interview_model:predict',data=data, headers=headers)
        predictions = json.loads(json_response.text)
        output = predictions.get("outputs")
        print(output)
        sort_index = np.argsort(np.array(output[0]))[::-1]
        res = {"data":list(pre_interview_label_list[sort_index]),"prob":np.array(output[0])[sort_index]}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')
    else:
        res = {"data": None}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')


@app.route('/inception/', methods=['POST'])
def inception_predict():
    s = request.get_json().get("s")
    if s:
        s_ids = assembly(s,inception_param[0],inception_param[1],inception_param[2])
        data = json.dumps({"inputs": {"input_data":s_ids}})
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://47.101.198.46:8501/v1/models/inception_model:predict',data=data, headers=headers)
        predictions = json.loads(json_response.text)
        output = predictions.get("outputs")
        mask = np.array(output[0])>= 0.5
        label = list(inception_label_list[mask])
        res = {"data":label}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')
    else:
        res = {"data": None}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')

@app.route('/cdss/', methods=['POST'])
def cdss_predict():
    s = request.get_json().get("s")
    if s:
        s_ids = assembly(s,inception_param[0],inception_param[1],inception_param[2])
        data = json.dumps({"inputs": {"input_data":s_ids}})
        headers = {"content-type": "application/json"}
        json_response = requests.post('http://47.101.198.46:8501/v1/models/multimodal:predict',data=data, headers=headers)
        predictions = json.loads(json_response.text)
        output = predictions.get("outputs")
        mask = np.array(output[0])>= 0.5
        label = list(inception_label_list[mask])
        res = {"data":label}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')
    else:
        res = {"data": None}
        return jsonify(res)
        # return Response(json.dumps(res), mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True)


