#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Write custom data generator for training and testing our CNN network for crop classification using label smoothing
#
######
from tensorflow.keras import preprocessing
import numpy as np
#np.random.seed(100)
import pandas as pd
from random import shuffle
#import tensorflow as tf
#from visualize import visualize_time_series
import pandas as pd
import json
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
from bert4keras.tokenizers import Tokenizer
from bert4keras.snippets import sequence_padding, DataGenerator

def set_embedding_matrix(vocab_file,word2vec_path):
    #加载预训练后的字向量
    # word_dic
    embeddings_index = {}
    with open(vocab_file,"r",encoding="utf8") as f:
        vocab = eval(f.readlines(1)[0])

    with open(word2vec_path,"r",encoding="utf8") as f:
        for wordvec in f.readlines():
            wordvec = wordvec.split(" ")
            wordvec[-1] = wordvec[-1].replace("\n","")
            word = wordvec[0]
            coefs = np.asarray(wordvec[1:], dtype="float32")
            embeddings_index[word] = coefs
    f.close()

    embedding_matrix = np.zeros((len(vocab) + 1, 100))
    print("embedding_matrix shape",embedding_matrix.shape)
    for word, i in vocab.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None:
            embedding_matrix[i] = embedding_vector

    with open("data/embedding","w",encoding="utf8") as f:
        for i in embedding_matrix:
            f.write(str(i)+"\n")
    return embedding_matrix




def data_converge(input_path,padding_size,num_classes):
    data_csv = pd.read_csv(input_path)
    
    structure_cols=['Out_Visit_Room_year', 'Out_Visit_Room_month', 'Out_Visit_Room_Day', 'Sex_Code', 'age', 'Visit_Dr_Code', 'Regist_Categ_Code','chest_belly_ep', 'head_ep', 'chest_ep', 'belly_ep', 'head_neck_spine_ep', 'heart_ep', 'superficial_ep', 'otorhinolaryngology_ep'] + data_csv.columns.tolist()[data_csv.columns.tolist().index('t_LIS036431'):-1]
    #structure_cols=['Out_Visit_Room_year', 'Out_Visit_Room_month', 'Out_Visit_Room_Day', 'Sex_Code', 'age', 'Visit_Dr_Code', 'Regist_Categ_Code','chest_belly_ep', 'head_ep', 'chest_ep', 'belly_ep', 'head_neck_spine_ep', 'heart_ep', 'superficial_ep', 'otorhinolaryngology_ep']    
    medical_record = 'medical_records'
    exam_result = 'exam_result'
    text_feature = data_csv[medical_record]+data_csv[exam_result]
    data_csv['text_cut'] = text_feature.apply(cut_word_character)
    scaler = StandardScaler()
    scaler.fit(data_csv[structure_cols])    
    
    text_cols = 'text_cut'
    label_col = 'diag_1st_label'   
    
    return_x_structure = data_csv[structure_cols].values
    return_x_structure = np.array(scaler.transform(return_x_structure))
    return_y = np.array(tf.one_hot(data_csv[label_col].values, num_classes))
    
    return_x_text = np.array(data_csv[text_cols])
    
    converged_data = [(return_x_text[index],structure_col,return_y[index]) for index,structure_col in enumerate(return_x_structure)]
    
    return converged_data



def preprocess_batches_using_bert(data_batch,padding_size,text_preprocesser,scaler,structure_cols,num_classes,batch_size,if_end,tokenizer):
    
    text_cols = 'text_cut'
    label_col = 'diag_1st_label'
    
    return_x_structure = data_batch[structure_cols].values
    return_x_structure = scaler.transform(return_x_structure)
    return_y = np.array(tf.one_hot(data_batch[label_col].values, num_classes))
    
    #处理text
    batch_token_ids, batch_segment_ids = [], []
    for index, row in data_batch.iterrows():
        token_ids, segment_ids = tokenizer.encode(row[text_cols], maxlen=padding_size)
        batch_token_ids.append(token_ids)
        batch_segment_ids.append(segment_ids)
        if if_end:
            if index == len(data_batch)-1:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
        else:
            if len(batch_token_ids) == batch_size:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)

    assert len(return_x_structure) == len(return_y), "TrainingX check exam features and Y lengths mismatch!"
    assert len(batch_token_ids) == len(return_y), "TrainingX medical records features and Y lengths mismatch!"
    assert len(batch_segment_ids) == len(return_y), "TrainingX medical records features and Y lengths mismatch!"
    return [return_x_structure, batch_token_ids, batch_segment_ids, return_y]


def preprocess_batches(data_batch,padding_size,text_preprocesser,scaler,structure_cols,num_classes):
    text_cols = 'text_cut'
    label_col = 'diag_1st_label'
    
    return_x_structure = data_batch[structure_cols].values
    return_x_structure = scaler.transform(return_x_structure)
    return_x_text = text_preprocesser.texts_to_sequences(data_batch[text_cols])
    return_x_text = preprocessing.sequence.pad_sequences(return_x_text, maxlen=padding_size,
                                                 padding='post', truncating='post')    
    return_y = tf.one_hot(data_batch[label_col].values, num_classes)
    
    assert len(return_x_structure) == len(return_y), "TrainingX check exam features and Y lengths mismatch!"
    assert len(return_x_text) == len(return_y), "TrainingX medical records features and Y lengths mismatch!"
    return [return_x_structure, return_x_text, return_y] 


def cut_word_character(word):
    return list(word)


def train_test_spliter(input_file,train_file,test_file):
    data_csv = pd.read_csv(input_file)
    train_data,test_data = train_test_split(data_csv,test_size=0.3, random_state=7)
    train_data.to_csv(train_file,index=False)
    test_data.to_csv(test_file,index=False)
    return True


def data_generator_udf(input_path,vocab_file,padding_size , batch_size=32, num_classes=2, do_shuffle=False, test=False,if_dump_vocab = False,if_use_bert = True):
    data_csv = pd.read_csv(input_path)
    if do_shuffle:
        shuffle(data_csv)
    
    structure_cols=['Out_Visit_Room_year', 'Out_Visit_Room_month', 'Out_Visit_Room_Day', 'Sex_Code', 'age', 'Visit_Dr_Code', 'Regist_Categ_Code','chest_belly_ep', 'head_ep', 'chest_ep', 'belly_ep', 'head_neck_spine_ep', 'heart_ep', 'superficial_ep', 'otorhinolaryngology_ep'] + data_csv.columns.tolist()[data_csv.columns.tolist().index('t_LIS036431'):-1]
    #structure_cols=['Out_Visit_Room_year', 'Out_Visit_Room_month', 'Out_Visit_Room_Day', 'Sex_Code', 'age', 'Visit_Dr_Code', 'Regist_Categ_Code','chest_belly_ep', 'head_ep', 'chest_ep', 'belly_ep', 'head_neck_spine_ep', 'heart_ep', 'superficial_ep', 'otorhinolaryngology_ep']    
    medical_record = 'medical_records'
    exam_result = 'exam_result'
    text_feature = data_csv[medical_record]+data_csv[exam_result]
    data_csv['text_cut'] = text_feature.apply(cut_word_character)
    scaler = StandardScaler()
    scaler.fit(data_csv[structure_cols])
    if not test:
        # Texts to sequences
        # first
        text_preprocesser = preprocessing.text.Tokenizer(filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~\t\n',
                                                             lower=True,
                                                             oov_token="<UNK>", split=' ')
        text_preprocesser.fit_on_texts(data_csv['text_cut'])
        if if_dump_vocab:
            #x = text_preprocesser.texts_to_sequences(data_csv['text_cut'])
            word_dict = text_preprocesser.word_index
            json.dump(word_dict, open(vocab_file, 'w', encoding="utf8"), ensure_ascii=False)
    #划分batch    
    curr_idx = 0
    if_end = False
    while True:
        # create random batches first
        #batch_paths = np.random.choice(a= all_images, size = batch_size)
        # initialize our batches of images and labels    
        if curr_idx >= len(data_csv): # reset if you've parsed all data
            curr_idx = 0
            if_end = False
        curr_batch = data_csv[curr_idx: (curr_idx + batch_size)]
        if if_use_bert:
            dict_path = 'D:\\python_workspace\\BERT\\mc_bert\\vocab.txt'  
            tokenizer = Tokenizer(dict_path, do_lower_case=True)
            if len(data_csv)-curr_idx<batch_size or curr_idx == len(data_csv):
                if_end = True
            check_exam_features, batch_token_ids, batch_segment_ids, labels = preprocess_batches_using_bert(curr_batch, padding_size, text_preprocesser, scaler, structure_cols, num_classes, batch_size, if_end, tokenizer)
            curr_idx += batch_size 
            yield [batch_token_ids, batch_segment_ids, check_exam_features], labels 
        else:
            check_exam_features, medical_record_features, labels = preprocess_batches(curr_batch,padding_size,text_preprocesser,scaler,structure_cols,num_classes) 
            curr_idx += batch_size 
            yield ([medical_record_features, check_exam_features], labels)

#_ = train_test_spliter('data/cough_data_all_features.csv', 'data/cough_data_all_features_train.csv', 'data/cough_data_all_features_test.csv')
#res = data_generator('data/cough_data_all_features.csv', 'data/vocab.txt', 291, batch_size=32, num_classes=27, do_shuffle=False, test=False,if_dump_vocab=False)
#test_data = data_generator('data/cough_data_all_features_test.csv', 'data/vocab.txt', 291, batch_size=32, num_classes=27, do_shuffle=False, test=False,if_dump_vocab=False)

#print()
#for a in test_data:
    #print(a)

