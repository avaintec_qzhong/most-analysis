#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
os.environ['TF_KERAS'] = '1'
from bert4keras.backend import keras
import sys
sys.path.append("..")
from bert4keras.models import build_transformer_model

def mutan_fusion(text_model, structure_model, num_classes, opt, structure_embedding = True, text_embedding = True):
    #基于bert4keras初始化input
    config_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_config.json'
    checkpoint_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_model.ckpt'    
    bert = build_transformer_model(
            config_path=config_path,
            checkpoint_path=checkpoint_path,
            model='bert',
            return_keras_model=False,
        )
    token_input,seg_input = bert.model.input
    
    #基于textcnn初始化input
    #text_input = keras.Input(shape=(opt['dim_t'],), name='input_data_mr')
    structure_input = keras.Input(shape=(opt['dim_s'],), name='input_data_fl')
    text_side = text_model([token_input,seg_input])
    structure_side = structure_model(structure_input)
    
    #处理structure_embedding和text_embedding
    if structure_embedding:
        x_s = keras.layers.Dense(opt['dim_hs'], activation=opt['activation_s'], name='structure_embedding',kernel_initializer='glorot_normal')(structure_side)
        x_s = keras.layers.Dropout(opt['dropout_s'], name='dropout_structure_embedding')(x_s)
    else:
        x_s = structure_side
        
    if text_embedding:
        x_t = keras.layers.Dense(opt['dim_ht'], activation=opt['activation_t'], name='text_embedding',kernel_initializer='glorot_normal')(text_side)
        x_t = keras.layers.Dropout(opt['dropout_t'], name='dropout_text_embedding')(x_t)
    else:
        x_t = text_side
    
    #处理秩R矩阵
    # 秩R的约束，（论文中）Z表示成R个Zr的总和（Z会投影到预测空间y上）。
    # 处理后的图像和问题，使用了对应位的相乘，
    # 使用堆叠求和方式进行相加，最终得到的x_mm相当于文章的Z
    x_mm = []
    
    if 'activation_hs' in opt:
        activation_hs = opt['activation_hs']
    else:
        activation_hs = None
    if 'activation_ht' in opt:
        activation_ht = opt['activation_ht']
    else:
        activation_ht = None    

    for i in range(opt['R']):
        x_hs = keras.layers.Dense(opt['dim_mm'], activation=activation_hs, name='dense_hs_'+str(i),kernel_initializer='glorot_normal')(x_s)      
        x_hs = keras.layers.Dropout(opt['dropout_hs'], name='dense_hv_dropout_'+str(i))(x_hs)
        
        
        x_ht = keras.layers.Dense(opt['dim_mm'], activation=activation_ht, name='dense_ht_'+str(i),kernel_initializer='glorot_normal')(x_t)
        x_ht = keras.layers.Dropout(opt['dropout_ht'], name='dense_ht_dropout_'+str(i))(x_ht)
        
        x_mm.append(keras.layers.multiply([x_ht, x_hs]))
    x_mm = keras.layers.add(x_mm)
    
    if 'activation_mm' in opt:
        x_mm = keras.layers.Activation(opt['activation_mm'])(x_mm)  
        
    outputs = keras.layers.Dense(num_classes, activation='softmax', name = "class")(x_mm)   
    
    model = keras.Model(inputs=[token_input,seg_input,structure_input], outputs=outputs)
    
    return model