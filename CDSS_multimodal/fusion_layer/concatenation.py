#!/usr/bin/python
# -*- coding: UTF-8 -*-
#from tensorflow import keras
import os
os.environ['TF_KERAS'] = '1'
from bert4keras.backend import keras
import sys
sys.path.append("..")
from bert4keras.models import build_transformer_model
#model = Concatenation(text_model, structure_model, text_input, structure_input, args.num_classes)
#def Concatenation(text_model,structure_model,text_input,structure_input,num_classes):
def Concatenation(text_model,structure_model,text_feature_size,structure_feature_size,num_classes):
    #config_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_config.json'
    #checkpoint_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_model.ckpt' 
    
    token_input = keras.Input(shape=(None,), name='token_input')
    seg_input = keras.Input(shape=(None,), name='seg_input')
    #bert = build_transformer_model(
            #config_path=config_path,
            #checkpoint_path=checkpoint_path,
            #model='bert',
            #return_keras_model=False,
        #)
    #token_input,seg_input = bert.model.input    
    
    
    #text_input = keras.Input(shape=(text_feature_size,), name='input_data_mr')
    structure_input = keras.Input(shape=(structure_feature_size,), name='input_data_fl')
    #text_side = text_model(text_input)
    text_side = text_model([token_input,seg_input])
    structure_side = structure_model(structure_input)
    # Concatenate features from images and texts
    merged = keras.layers.Concatenate()([text_side, structure_side])
    outputs = keras.layers.Dense(num_classes, activation='softmax', name = "class")(merged)    

    #model = keras.Model(inputs=[text_input,structure_input], outputs=outputs)
    model = keras.Model(inputs=[token_input,seg_input,structure_input], outputs=outputs)
    return model

