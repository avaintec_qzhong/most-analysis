#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
os.environ['TF_KERAS'] = '1'
from bert4keras.backend import keras
#from bert4keras.backend import keras
from bert4keras.models import build_transformer_model
from keras.layers import Lambda

def mc_bert(config_path,checkpoint_path):
    #token_input = keras.Input(shape=(None,), name='token_input')
    #seg_input = keras.Input(shape=(None,), name='seg_input')    
    bert = build_transformer_model(
        config_path=config_path,
        checkpoint_path=checkpoint_path,
        model='bert',
        return_keras_model=False,
    )
    output = Lambda(lambda x: x[:, 0], name='CLS-token')(bert.model.output)
    model = keras.models.Model(bert.model.input, output)
    #model = keras.models.Model([token_input,seg_input], output)
    return model