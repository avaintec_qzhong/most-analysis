#!/usr/bin/python
# -*- coding: UTF-8 -*-
from tensorflow import keras

def mr_DNN(feature_size, dropout_rate):
    inputs = keras.Input(shape=(feature_size,), name='input_data_mr')

    dnn = keras.layers.Dense(256, activation='relu', name='dense_1_mr',kernel_initializer='glorot_normal')(inputs)
    dnn_dropout = keras.layers.Dropout(dropout_rate, name='dropout_1_mr')(dnn)
    dnn_2 = keras.layers.Dense(128, activation='relu', name='dense_2_mr',kernel_initializer='glorot_normal')(dnn_dropout)
    outputs = keras.layers.Dropout(dropout_rate, name='dropout_2_mr')(dnn_2)
    #outputs = keras.layers.Dense(num_classes, activation='softmax',
                                 #kernel_initializer='glorot_normal',
                                 #bias_initializer=keras.initializers.constant(0.1),
                                 #kernel_regularizer=keras.regularizers.l2(regularizers_lambda),
                                 #bias_regularizer=keras.regularizers.l2(regularizers_lambda),
                                 #name='dense')(dnn)


    model = keras.Model(inputs=inputs, outputs=outputs)
    return model,inputs

