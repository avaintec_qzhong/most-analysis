#!/usr/bin/python
# -*- coding: UTF-8 -*-
from tensorflow.keras.callbacks import EarlyStopping

#from medical_record_layer import textcnn_model_builder
from physical_library_layer.dnn_model_builder_pl import pl_DNN
from medical_record_layer.textcnn_model_builder import TextCNN
from medical_record_layer.mc_bert_using_b4k import mc_bert
from fusion_layer.concatenation import Concatenation
from fusion_layer.mutan_keras import mutan_fusion
import data_processor
from bert4keras.tokenizers import Tokenizer
import argparse
from tensorflow import keras
import tensorflow as tf
from pprint import pprint
from tensorflow.keras import backend as K
import json
from bert4keras.snippets import sequence_padding, DataGenerator
import yaml
import utils
import numpy as np

def multi_category_focal_loss2(gamma=2., alpha=.25):
    """
    focal loss for multi category of multi label problem
    适用于多分类或多标签问题的focal loss
    alpha控制真值y_true为1/0时的权重
        1的权重为alpha, 0的权重为1-alpha
    当你的模型欠拟合，学习存在困难时，可以尝试适用本函数作为loss
    当模型过于激进(无论何时总是倾向于预测出1),尝试将alpha调小
    当模型过于惰性(无论何时总是倾向于预测出0,或是某一个固定的常数,说明没有学到有效特征)
        尝试将alpha调大,鼓励模型进行预测出1。
    Usage:
     model.compile(loss=[multi_category_focal_loss2(alpha=0.25, gamma=2)], metrics=["accuracy"], optimizer=adam)
    """
    epsilon = 1.e-7
    gamma = float(gamma)
    alpha = tf.constant(alpha,dtype=tf.float32)

    def multi_category_focal_loss2_fixed(y_true, y_pred):
        y_true = tf.cast(y_true, tf.float32)
        #防止log无限小 导致出现nan情况
        y_pred = tf.clip_by_value(y_pred, epsilon, 1. - epsilon)
        #alpha为正样本权重
        alpha_t = y_true * alpha + (tf.ones_like(y_true) - y_true) * (1 - alpha)
        y_t = tf.multiply(y_true, y_pred) + tf.multiply(1 - y_true, 1 - y_pred)
        ce = -tf.math.log(y_t)
        #1-pt 为难易区分程度的权重
        weight = tf.pow(tf.subtract(1., y_t), gamma)
        fl = tf.multiply(tf.multiply(weight, ce), alpha_t)
        loss = tf.reduce_mean(fl)
        return loss

    return multi_category_focal_loss2_fixed

class data_generator(DataGenerator):
    """数据生成器
    """
    def __iter__(self, random=False):
        batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
        for is_end, (text, structure_fs, label) in self.sample(random):
            token_ids, segment_ids = tokenizer.encode(text, maxlen=args.text_length)
            batch_token_ids.append(token_ids)
            batch_segment_ids.append(segment_ids)
            batch_structure_features.append(structure_fs)
            batch_labels.append(label)
            if len(batch_token_ids) == self.batch_size or is_end:
                batch_token_ids = sequence_padding(batch_token_ids)
                batch_segment_ids = sequence_padding(batch_segment_ids)
                batch_labels = sequence_padding(batch_labels)
                yield [ np.array(batch_token_ids), np.array(batch_segment_ids), np.array(batch_structure_features)], batch_labels
                batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []


def train(save_path,vocab_size,embedding_matrix,fusion_layer,tokenizer):
    print("\nTrain...")
    options = {}
    with open('conf/mutan_noatt_train.yaml', 'r') as conf:
        options_yaml = yaml.load(conf)
    options = utils.update_values(options, options_yaml)    
    
    structure_model = pl_DNN(args.structure_feature_dim, 0.5,args.num_classes)
    #用textcnn
    #text_model = TextCNN(vocab_size, args.text_length, args.embedding_dims, args.num_classes, args.num_filters, args.filter_sizes, args.regularizers_lambda, args.dropout_rate, embedding_matrix)
    
    config_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_config.json'
    checkpoint_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_model.ckpt' 
      
    text_model = mc_bert(config_path, checkpoint_path)
    
    train_data = data_processor.data_converge('data/cough_data_all_features_train.csv', args.text_length, args.num_classes)
    test_data = data_processor.data_converge('data/cough_data_all_features_test.csv', args.text_length, args.num_classes)
    train_generator = data_generator(train_data, args.batch_size,tokenizer)
    test_generator = data_generator(test_data, args.batch_size,tokenizer)    
    #train_data = data_processor.data_generator_udf('data/cough_data_all_features_train.csv', 'data/vocab.txt', args.text_length, batch_size=args.batch_size, num_classes=args.num_classes, do_shuffle=False, test=False,if_dump_vocab=False)
    #test_data = data_processor.data_generator_udf('data/cough_data_all_features_test.csv', 'data/vocab.txt', args.text_length, batch_size=args.batch_size, num_classes=args.num_classes, do_shuffle=False, test=False,if_dump_vocab=False)
    if fusion_layer == 'concatenation':
        model = Concatenation(text_model,structure_model,args.text_length,args.structure_feature_dim,args.num_classes)
    elif fusion_layer == 'mutan_fusion':
        model = mutan_fusion(text_model, structure_model, args.num_classes, options['model']['fusion'], structure_embedding = True, text_embedding = True)
    
    text_model.summary()
    structure_model.summary()
    model.summary()
    model.compile(tf.optimizers.Adam(), loss='categorical_crossentropy',metrics=['accuracy'])
    #model.compile(tf.optimizers.Adam(), loss=multi_category_focal_loss2(alpha=0.75, gamma=2.0),metrics=['accuracy'])

    EarlyStop = EarlyStopping(monitor='val_accuracy',patience=10, verbose=1, mode='auto')
    
    #validation_split在前，shuffle在后，如果样本本身不随机则需要先手动shuffle
    history = model.fit(train_generator.forfit(),
          epochs=1,
          steps_per_epoch = (1293//args.batch_size)+1,
          validation_data = test_generator.forfit(),
          validation_steps = (566//args.batch_size)+1,
          callbacks=[EarlyStop])
          
    
    print("\nSaving model...")
    #keras.models.save_model(model, save_path, save_format="tf")
    keras.models.save_model(model, save_path)
    pprint(history.history)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This is the HAN train project.')
    parser.add_argument('--num_classes', default=27, type=int, help='num of label classes')
    parser.add_argument('--text_length', default=291, type=int, help='dim of medical record')
    parser.add_argument('--dropout_rate', default=0.5, type=float, help='Dropout rate in softmax layer.(default=0.3)')
    parser.add_argument('--regularizers_lambda', default=0.01, type=float, help='L2 regulation parameter.(default=0.01)')
    parser.add_argument('--filter_sizes', default='3,4,5', help='Convolution kernel sizes.(default=3,4,5)')
    parser.add_argument('--structure_feature_dim', default=423, type=int, help='dim of physical library')
    #parser.add_argument('--structure_feature_dim', default=15, type=int, help='dim of physical library')
    parser.add_argument('--maxlen_sentence', default=5, type=int, help='max length of sentence')
    parser.add_argument('--num_filters', default=256, type=int, help='Number of each convolution kernel.(default=128)')
    parser.add_argument('--maxlen_word', default=40, type=int, help='length of padded words')
    parser.add_argument('--batch_size', default=10,type=int, help='batch size')
    parser.add_argument('--embedding_dims', default=100, type=int, help='num of embedding dim')
    parser.add_argument('--epochs', default=10, type=int, help='Number of epochs.(default=10)')
    parser.add_argument('--fraction_validation', default=0.05, type=float, help='The fraction of validation.(default=0.05)')
    args = parser.parse_args()
    print('Parameters:', args, '\n')

    vocab_file = r"data/vocab.txt"
    embedding_path = r"model/medical_token_vec_100.bin"
    vocab = json.load(open(vocab_file, 'r',encoding="utf8"))
    vocab_size = len(vocab)

    embedding_matrix = data_processor.set_embedding_matrix(vocab_file,embedding_path)
    dict_path = 'D:\\python_workspace\\BERT\\mc_bert\\vocab.txt'
    tokenizer = Tokenizer(dict_path, do_lower_case=True)
    
    save_path = 'model/multimodal_mcbert.h5'
     
    train(save_path,vocab_size,embedding_matrix,'mutan_fusion',tokenizer)
    #train(save_path,vocab_size,embedding_matrix,'concatenation')
