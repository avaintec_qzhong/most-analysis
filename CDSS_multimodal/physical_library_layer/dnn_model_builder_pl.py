#!/usr/bin/python
# -*- coding: UTF-8 -*-
from tensorflow import keras

def pl_DNN(feature_size, dropout_rate,num_classes):
    inputs = keras.Input(shape=(feature_size,), name='input_data_pl')

    dnn = keras.layers.Dense(256, activation='relu', name='dense_1_pl',kernel_initializer='glorot_normal')(inputs)
    dnn_dropout = keras.layers.Dropout(dropout_rate, name='dropout_1_pl')(dnn)
    dnn_2 = keras.layers.Dense(128, activation='relu', name='dense_2_pl',kernel_initializer='glorot_normal')(dnn_dropout)
    outputs = keras.layers.Dropout(dropout_rate, name='dropout_2_pl')(dnn_2)
    #outputs = keras.layers.Dense(num_classes, activation='softmax',
                                 #kernel_initializer='glorot_normal',
                                 #bias_initializer=keras.initializers.constant(0.1),
                                 #kernel_regularizer=keras.regularizers.l2(regularizers_lambda),
                                 #bias_regularizer=keras.regularizers.l2(regularizers_lambda),
                                 #name='dense')(outputs)


    model = keras.Model(inputs=inputs, outputs=outputs)
    return model

