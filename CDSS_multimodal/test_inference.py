#!/usr/bin/python
# -*- coding: UTF-8 -*-
from physical_library_layer.dnn_model_builder_pl import pl_DNN
from medical_record_layer.mc_bert_using_b4k import mc_bert
from fusion_layer.mutan_keras import mutan_fusion
from bert4keras.tokenizers import Tokenizer
import yaml
import utils
import numpy as np
from main import data_generator
import data_processor
from bert4keras.snippets import sequence_padding, DataGenerator
import tensorflow as tf
import json



#test
def prediction_lookup(sorted_list):
    init_config='conf/label_name.json'
    with open(init_config, 'r', encoding='utf-8') as f:
        label_dict = json.loads(f.read())  

    prediction = [label_dict.get(str(item)) for item in sorted_list]
    
    return prediction

sorted_list = [0, 6, 21, 11, 3, 15, 7, 4, 1, 2, 10, 13, 23, 20, 26, 25, 22, 18, 12, 16, 17, 5, 24, 14, 9, 8, 19]


print()

#class data_generator(DataGenerator):
    #"""数据生成器
    #"""
    #def __iter__(self, random=False):
        #batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
        #for is_end, (text, structure_fs, label) in self.sample(random):
            #token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
            #batch_token_ids.append(token_ids)
            #batch_segment_ids.append(segment_ids)
            #batch_structure_features.append(structure_fs)
            #batch_labels.append(label)
            #if len(batch_token_ids) == self.batch_size or is_end:
                #batch_token_ids = sequence_padding(batch_token_ids)
                #batch_segment_ids = sequence_padding(batch_segment_ids)
                #batch_labels = sequence_padding(batch_labels)
                #return np.array(batch_token_ids), np.array(batch_segment_ids), np.array(batch_structure_features)

def xstr(s):
    return '' if s is None else str(s)+'。'


def check_processor_fn(feature:dict):
    exam_feature = '检查项目：'+xstr(feature['item'])+'检查描述：'+xstr(feature['describe'])+'检查结果：'+xstr(feature['result'])

    return exam_feature


def structured_feature_processor_fn(feature:list,init_config): #feature : {name:"z"}

    #columns_list = pd.read_csv(r"E:\zhj\most_develop\service\text\cough_data_all_features_validation(1).csv").columns.tolist()
    #columns_dict = {i:0 for i in columns_list}
    with open(init_config, 'r', encoding='utf-8') as f:
        columns_dict = json.loads(f.read())    

    #本地调试
    #for k,v in feature.items():
        #if columns_dict.get(k):
            #columns_dict[k] = 1
            
    for items in feature:
        if columns_dict.get(items['item']):
            columns_dict[items['item']] = 1

    #服务器调试
    # for k,v in feature.items():
    #     feature_name = k+"_"+v
    #     if columns_dict.get(k):
    #         columns_dict[k] = 1
    
    #for items in feature:
        #feature_name = items['item']+"_"+items['belong']
        #if columns_dict.get(feature_name):
            #columns_dict[feature_name] = 1    
    structured_feature = np.array(list(columns_dict.values()))
    return structured_feature


def data_init(structure_data,exam_data,mr_data,init_config):
    #处理结构化特征
    #with open(init_config, 'r', encoding='utf-8') as f:
        #init_conf_json = json.loads(f.read())
    
    #for items in structure_data:
        #if init_conf_json.get(items['item']):
            #init_conf_json[items['item']] = 1
    
    #structure_feature = np.array(list(init_conf_json.values()))
    structure_feature = structured_feature_processor_fn(structure_data,init_config)
    
    #处理检查+病历 文本类特征
    exam_feature = ''
    for items in exam_data:
        exam_feature += check_processor_fn(items)
    
    text_feature = np.array(list(mr_data + exam_feature))
    
    return [(text_feature,structure_feature)]

def data_generator(data, tokenizer):
    #batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
    batch_token_ids, batch_segment_ids,batch_structure_features = [], [], []
    #text,structure_fs, label = data[0]
    text,structure_fs = data[0]
    token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    #batch_labels.append(label)
    batch_token_ids = sequence_padding(batch_token_ids)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    #batch_labels = sequence_padding(batch_labels)
    return np.array(batch_token_ids), np.array(batch_segment_ids), np.array(batch_structure_features)    


def data_generator_tfserving(data, tokenizer):
    #batch_token_ids, batch_segment_ids,batch_structure_features, batch_labels = [], [], [], []
    batch_token_ids, batch_segment_ids,batch_structure_features = [], [], []
    #text,structure_fs, label = data[0]
    text,structure_fs = data[0]
    token_ids, segment_ids = tokenizer.encode(text, maxlen=291)
    batch_token_ids.append(token_ids)
    batch_segment_ids.append(segment_ids)
    batch_structure_features.append(structure_fs)
    #batch_labels.append(label)
    batch_token_ids = sequence_padding(batch_token_ids)
    batch_segment_ids = sequence_padding(batch_segment_ids)
    #batch_labels = sequence_padding(batch_labels)
    json_data = json.dumps({"inputs": {"input_data_fl":[structure_fs.tolist()] ,"Input-Token":[token_ids],"Input-Segment":batch_segment_ids.tolist()}})
    return json_data



options = {}
with open('conf/mutan_noatt_train.yaml', 'r') as conf:
    options_yaml = yaml.load(conf)
options = utils.update_values(options, options_yaml)       

structure_model = pl_DNN(423, 0.5,27)
#用textcnn
#text_model = TextCNN(vocab_size, args.text_length, args.embedding_dims, args.num_classes, args.num_filters, args.filter_sizes, args.regularizers_lambda, args.dropout_rate, embedding_matrix)

config_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_config.json'
checkpoint_path = 'D:\\python_workspace\\BERT\\mc_bert\\bert_model.ckpt' 
dict_path='D:\\python_workspace\\BERT\\mc_bert\\vocab.txt' 

tokenizer = Tokenizer(dict_path, do_lower_case=True)

text_model = mc_bert(config_path, checkpoint_path)

#valid_data = data_processor.data_converge('data/cough_data_all_features_validation.csv', 291, 27)

structure_data = [{'time': '2021-05-01', 'item': 'test002', 'belong': 'z', 'result': '正常', 'unit': 'mol', 'reference_value': '0.5-6', 'preInquiry_id': 'id2021-04-30-1'}]
exam_data = [{'time': '2021-05-01', 'item': 'test001', 'position': 'asdwe', 'describe': '123412', 'result': '正常', 'preInquiry_id': 'id2021-04-30-1'}, {'time': '2021-05-01', 'item': 'test001', 'position': 'asdwe', 'describe': '123412', 'result': '正常', 'preInquiry_id': 'id2021-04-30-1'}, {'time': None, 'item': 'test003', 'position': None, 'describe': None, 'result': '正常', 'preInquiry_id': 'id2021-04-30-1'}]
mr_data = '主诉:咳嗽2月现病史:2月前咳嗽，有痰，晨起明显，无喘，无流涕，无发热。食欲可。无异物呛咳史既往史:有鼻炎'
init_config = 'conf/init_config.json'
valid_data = data_init(structure_data, exam_data, mr_data, init_config)

valid_generator = data_generator(valid_data, tokenizer)  

valid_generator_json = data_generator_tfserving(valid_data, tokenizer)

model = mutan_fusion(text_model, structure_model, 27, options['model']['fusion'], structure_embedding = True, text_embedding = True)    
#train(save_path,vocab_size,embedding_matrix,'concatenation')
model.load_weights('model/multimodal_mcbert.h5')

#pb_model = tf.saved_model.load('model/multimodal')
res = model.predict(valid_generator)
print()
    
