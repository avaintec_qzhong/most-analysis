# README #

### What is this repository for? ###

MOST辅助决策模型相关程序

### what are the folds for ###

conf: 配置文件

data：存放数据的目录

fusion_layer：特征融合层代码目录

medical_record_layer：病历特征处理层代码目录

model：存放模型目录

physical_library_layer：结构化特征层代码目录

### The function of python footages ###
fusion_layer下：
concatenation.py: 使用concat方法特征融合的程序
mutan_keras.py: 使用mutan fusion融合特征的程序

medical_record_layer下：
mc_bert_using_b4k.py: 使用mc bert提取文本特征的程序
textcnn_model_builder.py: 使用textcnn提取文本特征的程序

physical_library_layer下：
dnn_model_builder_pl.py: 使用dnn提取结构化特征的程序


data_assembly.py: 部署服务时处理数据的程序
data_processor.py: 训练模型时数据处理的程序
flask_api.py: 部署接口服务的程序
main.py: 训练方法
test_inference.py: 预测方法
utils.py: 工具类

### How do I get set up? ###
模型训练：python main.py
模型预测：python test_inference.py




