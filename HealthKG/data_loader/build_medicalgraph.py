#!/usr/bin/env python3
#coding:utf-8
#初始化数据程序，使用load csv命令导入实体和关系到neo4j
import csv
import os
import json

class MedicalGraph:
    def __init__(self):
        cur_dir = '/'.join(os.path.abspath(__file__).split('/')[:-1])
        self.data_path = os.path.join(cur_dir,'..','data/medical_20190624.json')
        
        #self.neo4j_data_path=os.path.join(cur_dir,'..','neo4j_format_data','disease_symptom_food')
        self.neo4j_data_path=os.path.join(cur_dir,'..','neo4j_format_data','disease_all_data')
        
        self.pid_start_int={
            'health_exam':1,
            'exam_items':2,
            'clinical_sign':3,
            'disease':4,
            'symptom':5,
            'department':6,
            'food':7,
            'check':8,
            'drug':9,
            'producer':10
        }

    '''读取文件'''
    def read_nodes(self):
        # 共７类节点
        drugs = [] # 药品
        foods = [] #　食物
        checks = [] # 检查
        departments = [] #科室
        producers = [] #药品大类
        diseases = [] #疾病
        symptoms = []#症状

        disease_infos = []#疾病信息

        # 构建节点实体关系
        rels_department = [] #　科室－科室关系
        rels_noteat = [] # 疾病－忌吃食物关系
        rels_doeat = [] # 疾病－宜吃食物关系
        rels_recommandeat = [] # 疾病－推荐吃食物关系
        rels_commonddrug = [] # 疾病－通用药品关系
        rels_recommanddrug = [] # 疾病－热门药品关系
        rels_check = [] # 疾病－检查关系
        rels_drug_producer = [] # 厂商－药物关系

        rels_symptom = [] #疾病症状关系
        rels_acompany = [] # 疾病并发关系
        rels_category = [] #　疾病与科室之间的关系


        count = 0
        for data in open(self.data_path,encoding='utf-8'):
            disease_dict = {}
            count += 1
            print(count)
            data_json = json.loads(data)
            disease = data_json['name']
            disease_dict['name'] = disease
            diseases.append(disease)
            disease_dict['desc'] = ''
            disease_dict['prevent'] = ''
            disease_dict['cause'] = ''
            disease_dict['easy_get'] = ''
            disease_dict['cure_department'] = ''
            disease_dict['cure_way'] = ''
            disease_dict['cure_lasttime'] = ''
            disease_dict['symptom'] = ''
            disease_dict['cured_prob'] = ''

            if 'symptom' in data_json:
                symptoms += data_json['symptom']
                for symptom in data_json['symptom']:
                    rels_symptom.append([disease, symptom])

            if 'acompany' in data_json:
                for acompany in data_json['acompany']:
                    rels_acompany.append([disease, acompany])

            if 'desc' in data_json:
                disease_dict['desc'] = data_json['desc']

            if 'prevent' in data_json:
                disease_dict['prevent'] = data_json['prevent']

            if 'cause' in data_json:
                disease_dict['cause'] = data_json['cause']

            if 'get_prob' in data_json:
                disease_dict['get_prob'] = data_json['get_prob']

            if 'easy_get' in data_json:
                disease_dict['easy_get'] = data_json['easy_get']

            if 'cure_department' in data_json:
                cure_department = data_json['cure_department']
                if len(cure_department) == 1:
                    rels_category.append([disease, cure_department[0]])
                if len(cure_department) == 2:
                    big = cure_department[0]
                    small = cure_department[1]
                    rels_department.append([small, big])
                    rels_category.append([disease, small])

                disease_dict['cure_department'] = cure_department
                departments += cure_department

            if 'cure_way' in data_json:
                disease_dict['cure_way'] = data_json['cure_way']

            if  'cure_lasttime' in data_json:
                disease_dict['cure_lasttime'] = data_json['cure_lasttime']

            if 'cured_prob' in data_json:
                disease_dict['cured_prob'] = data_json['cured_prob']

            if 'common_drug' in data_json:
                common_drug = data_json['common_drug']
                for drug in common_drug:
                    rels_commonddrug.append([disease, drug])
                drugs += common_drug

            if 'recommand_drug' in data_json:
                recommand_drug = data_json['recommand_drug']
                drugs += recommand_drug
                for drug in recommand_drug:
                    rels_recommanddrug.append([disease, drug])

            if 'not_eat' in data_json:
                not_eat = data_json['not_eat']
                for _not in not_eat:
                    rels_noteat.append([disease, _not])

                foods += not_eat
                do_eat = data_json['do_eat']
                for _do in do_eat:
                    rels_doeat.append([disease, _do])

                foods += do_eat
                recommand_eat = data_json['recommand_eat']

                for _recommand in recommand_eat:
                    rels_recommandeat.append([disease, _recommand])
                foods += recommand_eat

            if 'check' in data_json:
                check = data_json['check']
                for _check in check:
                    rels_check.append([disease, _check])
                checks += check
            if 'drug_detail' in data_json:
                drug_detail = data_json['drug_detail']
                producer = [i.split('(')[0] for i in drug_detail]
                rels_drug_producer += [[i.split('(')[0], i.split('(')[-1].replace(')', '')] for i in drug_detail]
                producers += producer
            disease_infos.append(disease_dict)
        return set(drugs), set(foods), set(checks), set(departments), set(producers), set(symptoms), set(diseases), disease_infos,\
               rels_check, rels_recommandeat, rels_noteat, rels_doeat, rels_department, rels_commonddrug, rels_drug_producer, rels_recommanddrug,\
               rels_symptom, rels_acompany, rels_category
    
    def add_pid(self,input_nodes,node_name):
        return
    
    
    #处理节点list，处理成能入库的形式
    def deal_nodes(self,input_nodes,node_name):
        if node_name=='disease':
            res = [[nodes['name'],nodes['desc'],nodes['prevent'],nodes['cause'],nodes['easy_get'],nodes['cure_lasttime'],
                    nodes['cure_department'],nodes['cure_way'],nodes['cured_prob']] for nodes in input_nodes]
        
        else:
            res=[[nodes] for nodes in input_nodes]
        
        return res
    
    
    
    '''导出实体文件，load csv格式'''
    def export_nodes_relations_to_csv(self,entity_list,output_path,csv_heads):
        with open(output_path,"w+",newline='',encoding='utf-8') as csvfile: 
            writer = csv.writer(csvfile)
            
            #先写入columns_name
            writer.writerow(csv_heads)
            #写入多行用writerows
            writer.writerows(entity_list)            
        return

   
    '''导关系和节点的控制器'''
    def export_handler(self):
        Drugs, Foods, Checks, Departments, Producers, Symptoms, Diseases, disease_infos, rels_check, rels_recommandeat, rels_noteat, rels_doeat, rels_department, rels_commonddrug, rels_drug_producer, rels_recommanddrug, rels_symptom, rels_acompany, rels_category = self.read_nodes()
            
        disease_node_path=os.path.join(self.neo4j_data_path,'disease_node.csv')
        symptom_node_path=os.path.join(self.neo4j_data_path,'symptom_node.csv')
        food_node_path=os.path.join(self.neo4j_data_path,'food_node.csv')
        department_node_path=os.path.join(self.neo4j_data_path,'department_node.csv')
        check_node_path=os.path.join(self.neo4j_data_path,'check_node.csv')
        drug_node_path=os.path.join(self.neo4j_data_path,'drug_node.csv')
        producer_node_path=os.path.join(self.neo4j_data_path,'producer_node.csv')
        
        recommand_eat_relations_path=os.path.join(self.neo4j_data_path,'recommand_eat_relations.csv')
        do_eat_relations_path=os.path.join(self.neo4j_data_path,'do_eat_relations.csv')
        not_eat_relations_path=os.path.join(self.neo4j_data_path,'not_eat_relations.csv')
        belongs_to_department_relations_path=os.path.join(self.neo4j_data_path,'belongs_to_department_relations.csv')
        has_symptom_relations_path=os.path.join(self.neo4j_data_path,'has_symptom_relations.csv')
        department_category_relations_path=os.path.join(self.neo4j_data_path,'department_category_relations.csv')
        common_drug_relations_path=os.path.join(self.neo4j_data_path,'common_drug_relations.csv')
        drugs_of_relations_path=os.path.join(self.neo4j_data_path,'drugs_of_relations.csv')
        recommand_drug_relations_path=os.path.join(self.neo4j_data_path,'recommand_drug_relations.csv')
        need_check_relations_path=os.path.join(self.neo4j_data_path,'need_check_relations.csv')
        acompany_with_relations_path=os.path.join(self.neo4j_data_path,'acompany_with_relations.csv')
        
        
        disease_infos=self.deal_nodes(disease_infos, 'disease')
        Symptoms=self.deal_nodes(Symptoms, 'symptom')
        Foods=self.deal_nodes(Foods, 'food')
        Drugs=self.deal_nodes(Drugs, 'drug')
        Checks=self.deal_nodes(Checks, 'check')
        Producers=self.deal_nodes(Producers, 'producer')
        Departments=self.deal_nodes(Departments, 'department')
        
        #导出实体节点csv
        self.export_nodes_relations_to_csv(disease_infos, disease_node_path, ['name','desc','prevent','cause','easy_get','cure_lasttime',
                                                                              'cure_department','cure_way','cured_prob'])
        self.export_nodes_relations_to_csv(Symptoms, symptom_node_path,['name'])
        self.export_nodes_relations_to_csv(Foods, food_node_path, ['name'])
        self.export_nodes_relations_to_csv(Departments, department_node_path, ['name'])
        self.export_nodes_relations_to_csv(Drugs, drug_node_path, ['name'])
        self.export_nodes_relations_to_csv(Producers, producer_node_path, ['name'])
        self.export_nodes_relations_to_csv(Checks, check_node_path, ['name'])
        
        #导出关系csv
        self.export_nodes_relations_to_csv(rels_recommandeat, recommand_eat_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_doeat, do_eat_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_noteat, not_eat_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_department, belongs_to_department_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_symptom, has_symptom_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_category, department_category_relations_path, ['entity1','entity2'])
        #common_drug需要单独用以前原作者处理的关系来生成才行
        #self.export_nodes_relations_to_csv(rels_commonddrug, common_drug_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_drug_producer, drugs_of_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_recommanddrug, recommand_drug_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_check, need_check_relations_path, ['entity1','entity2'])
        self.export_nodes_relations_to_csv(rels_acompany, acompany_with_relations_path, ['entity1','entity2'])
        return    


if __name__ == '__main__':
    handler = MedicalGraph()
    handler.export_handler()
    #handler.export_data()
