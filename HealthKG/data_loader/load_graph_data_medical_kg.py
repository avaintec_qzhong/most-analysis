#!/usr/bin/env python3
#coding:utf-8
#初始化数据程序，使用load csv命令导入实体和关系到neo4j
import os
import json
from py2neo import Graph,Node


class HealthExaminationGraph:
    def __init__(self):
        cur_dir='/'.join(os.path.abspath(__file__).split('/')[:-1])
        #生产环境
        #self.graph = Graph(
            #host="127.0.0.1",
            #http_port=7474,
            #user="neo4j",
            #password="T9MBG7TpAVAaKPR68HZO")
        #本地环境
        self.graph = Graph(
            host="127.0.0.1",
            http_port=7474,
            user="neo4j",
            password="@Shield123456")        
    
    '''
    函数功能：创建neo4j节点
    参数列表：
    file_name:需要load到neo4j的图的csv文件名
    label:load到neo4j对应的label名
    properties:属性列表，列表中存放节点的所有属性名
    functions:传递函数列表，列表长度为len(properties)，表示load某个属性的值时需要调用的函数名，不需要函数时值为None
    '''    
    def build_nodes(self,file_name,label,properties,functions):
        property_format_list = []
        for p_index in range(len(properties)):
            if functions[p_index]=='None':
                property_format_list.append(properties[p_index]+':line.'+properties[p_index])
            else:
                property_format_list.append(properties[p_index]+':'+functions[p_index]+'('+'line.'+ properties[p_index] +')')
        property_format_list=','.join(property_format_list)
        query = "load csv with headers from 'file:///%s' as line create (:%s{%s})" %(file_name,label,property_format_list)
        try:
            res = self.graph.run(query)
        except Exception as e:
            print(e)
        return
    
    '''
    函数功能：创建neo4j关系
    参数列表：
    file_name:需要load到neo4j关系的csv文件名
    node1:左节点label名称
    node2:右节点label名称
    node1_properties:左节点的关联属性，类型为dict，key为节点的属性名，value为csv文件中node1属性表头名称
    node2_properties:右节点关联属性，同node1
    relation_name:关系名，csv文件中关系的表头
    '''
    def build_relation(self,file_name,node1,node2,node1_properties,node2_properties,relation_name):
        entity1_property_format=','.join([key+':line.'+node1_properties[key] for key in node1_properties.keys()])
        entity2_property_format=','.join([key+':line.'+node2_properties[key] for key in node2_properties.keys()])
        
        query="load csv with headers from 'file:///%s' as line match (entity1:%s{%s}),(entity2:%s{%s}) " \
        "create (entity1)-[:%s{r:line.relation}]->(entity2)" %(
            file_name,node1,entity1_property_format,node2,entity2_property_format,relation_name
        )
        try:
            res = self.graph.run(query)
        except Exception as e:
            print(e)
        
        return
    

#实例化对象
handler = HealthExaminationGraph()
'''创建疾病、症状、科室、食物、检查项、药品、药品生产厂商节点及对应关系'''
#创建节点
#handler.build_nodes('distinct_nodes/disease_node.csv','Disease',['pid','name','desc','prevent','cause','easy_get','cure_lasttime','cure_department','cure_way','cured_prob'],
                    #['None','None','None','None','None','None','None','None','None','None'])
#handler.build_nodes('distinct_nodes/symptom_node.csv','Symptom',['pid','name',],['None','None'])
#handler.build_nodes('distinct_nodes/food_node.csv','Food',['pid','name',],['None','None'])
#handler.build_nodes('distinct_nodes/department_node.csv','Department',['pid','name',],['None','None'])
#handler.build_nodes('distinct_nodes/check_node.csv','Check',['pid','name',],['None','None'])
#handler.build_nodes('distinct_nodes/drug_node.csv','Drug',['pid','name',],['None','None'])
#handler.build_nodes('distinct_nodes/producer_node.csv','Producer',['pid','name',],['None','None'])

#创建关系
#handler.build_relation('distinct_rels/has_symptom_relations.csv', 'Disease', 'Symptom', {'name':'entity1'}, {'name':'entity2'}, 'has_symptom')
handler.build_relation('distinct_rels/belongs_to_department_relations.csv', 'Department', 'Department', {'name':'entity1'}, {'name':'entity2'}, 'belongs_to_department')
handler.build_relation('distinct_rels/department_category_relations.csv', 'Disease', 'Department', {'name':'entity1'}, {'name':'entity2'}, 'department_category')
handler.build_relation('distinct_rels/do_eat_relations.csv', 'Disease', 'Food', {'name':'entity1'}, {'name':'entity2'}, 'do_eat')
handler.build_relation('distinct_rels/not_eat_relations.csv', 'Disease', 'Food', {'name':'entity1'}, {'name':'entity2'}, 'not_eat')
handler.build_relation('distinct_rels/recommand_eat_relations.csv', 'Disease', 'Food', {'name':'entity1'}, {'name':'entity2'}, 'recommand_eat')

handler.build_relation('distinct_rels/acompany_with_relations.csv', 'Disease', 'Disease', {'name':'entity1'}, {'name':'entity2'}, 'acompany_with')
handler.build_relation('distinct_rels/common_drug_relations.csv', 'Disease', 'Drug', {'name':'entity1'}, {'name':'entity2'}, 'common_drug')
handler.build_relation('distinct_rels/drugs_of_relations.csv', 'Producer', 'Drug', {'name':'entity1'}, {'name':'entity2'}, 'drug_of')
handler.build_relation('distinct_rels/need_check_relations.csv', 'Disease', 'Check', {'name':'entity1'}, {'name':'entity2'}, 'need_check')
handler.build_relation('distinct_rels/recommand_drug_relations.csv', 'Disease', 'Drug', {'name':'entity1'}, {'name':'entity2'}, 'recommand_drug')


