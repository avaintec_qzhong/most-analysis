# README #

### What is this repository for? ###

MOST 医疗百科问答

### what are the folds for ###

conf: 配置文件

data：存放数据的目录

fusion_layer：特征融合层代码目录

medical_record_layer：病历特征处理层代码目录

model：存放模型目录

physical_library_layer：结构化特征层代码目录

### The function of python footages ###
data_loader下：
build_medicalgraph.py: 构建医疗知识图谱程序（解析json的途径）
load_graph_data.py: 构建医疗知识图谱体检部分程序（load csv）
load_graph_data_medical_kg.py: 构建医疗知识图谱主体部分程序（load csv）


distinct_relations.py: 关系去重


### How do I get set up? ###
用json解析的途径导入数据：python build_medicalgraph.py
用load csv的途径导入数据：python load_graph_data_medical_kg.py




