#!/usr/bin/env python3
#coding:utf-8
#通用工具函数

#检查字符串中内容是否是浮点型小数，是返回True，否返回False
def is_float(aString):
    try:
        float(aString)
        return True
    
    except:
        return False

#根据更换key字典更换输入字典的key
def change_dict_key(input_dict,changekey_dict):
    for input_key in input_dict.keys():
        if input_key in changekey_dict.keys():
            input_dict[changekey_dict[input_key]] = input_dict.pop(input_key)
    
    return
