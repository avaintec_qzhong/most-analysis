#!/usr/bin/env python3
# coding: utf-8
import csv

def export_relations_to_csv(entity_list,output_path):
    with open(output_path,"w+",newline='',encoding='utf-8') as csvfile: 
        writer = csv.writer(csvfile)
        
        #先写入columns_name
        #writer.writerow(csv_heads)
        #写入多行用writerows
        writer.writerows(entity_list)            
    return

def read_relations_from_csv(csv_file):
    with open(csv_file,'r',encoding='utf-8') as csv_file:
        res = [lines.strip().split(',') for lines in csv_file]
    
    return res

def distinct_relations(input_list):
    res = []
    for item in input_list:
        if item not in res:
            res.append(item)    
    
    return res


def ditinct_handler(input_csv_file,output_csv_file):
    rels_list = read_relations_from_csv(input_csv_file)
    rels_list = distinct_relations(rels_list)
    
    export_relations_to_csv(rels_list, output_csv_file)
    return 

input_path='neo4j_format_data/disease_all_data/'
output_path='neo4j_format_data/disease_all_data/distinct_rels/'

cd_input=input_path+'common_drug_relations.csv'
aw_input=input_path+'acompany_with_relations.csv'
do_input=input_path+'drugs_of_relations.csv'
nc_input=input_path+'need_check_relations.csv'
rd_input=input_path+'recommand_drug_relations.csv'
btd_input=input_path+'belongs_to_department_relations.csv'
dc_input=input_path+'department_category_relations.csv'
de_input=input_path+'do_eat_relations.csv'
hs_input=input_path+'has_symptom_relations.csv'
ne_input=input_path+'not_eat_relations.csv'
re_input=input_path+'recommand_eat_relations.csv'

cd_output=output_path+'common_drug_relations.csv'
aw_output=output_path+'acompany_with_relations.csv'
do_output=output_path+'drugs_of_relations.csv'
nc_output=output_path+'need_check_relations.csv'
rd_output=output_path+'recommand_drug_relations.csv'
btd_output=output_path+'belongs_to_department_relations.csv'
dc_ouput=output_path+'department_category_relations.csv'
de_ouput=output_path+'do_eat_relations.csv'
hs_ouput=output_path+'has_symptom_relations.csv'
ne_ouput=output_path+'not_eat_relations.csv'
re_ouput=output_path+'recommand_eat_relations.csv'

#cd_rels = ditinct_handler(cd_input,cd_output)
#aw_rels = ditinct_handler(aw_input,aw_output)
#do_rels = ditinct_handler(do_input,do_output)
#nc_rels = ditinct_handler(nc_input,nc_output)
#rd_rels = ditinct_handler(rd_input,rd_output)
#btd_rels = ditinct_handler(btd_input,btd_output)
#dc_rels = ditinct_handler(dc_input,dc_ouput)
#de_rels = ditinct_handler(de_input,de_ouput)
#hs_rels = ditinct_handler(hs_input,hs_ouput)
#ne_rels = ditinct_handler(ne_input,ne_ouput)
#re_rels = ditinct_handler(re_input,re_ouput)

bf_cd_rels = read_relations_from_csv(cd_input)
bf_aw_rels = read_relations_from_csv(aw_input)
bf_do_rels = read_relations_from_csv(do_input)
bf_nc_rels = read_relations_from_csv(nc_input)
bf_rd_rels = read_relations_from_csv(rd_input)
bf_btd_rels = read_relations_from_csv(btd_input)
bf_dc_rels = read_relations_from_csv(dc_input)
bf_de_rels = read_relations_from_csv(de_input)
bf_hs_rels = read_relations_from_csv(hs_input)
bf_ne_rels = read_relations_from_csv(ne_input)
bf_re_rels = read_relations_from_csv(re_input)

aft_cd_rels = read_relations_from_csv(cd_output)
aft_aw_rels = read_relations_from_csv(aw_output)
aft_do_rels = read_relations_from_csv(do_output)
aft_nc_rels = read_relations_from_csv(nc_output)
aft_rd_rels = read_relations_from_csv(rd_output)
aft_btd_rels = read_relations_from_csv(btd_output)
aft_dc_rels = read_relations_from_csv(dc_ouput)
aft_de_rels = read_relations_from_csv(de_ouput)
aft_hs_rels = read_relations_from_csv(hs_ouput)
aft_ne_rels = read_relations_from_csv(ne_ouput)
aft_re_rels = read_relations_from_csv(re_ouput)

print()